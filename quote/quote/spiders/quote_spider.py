import scrapy

from scrapy.selector import Selector

from quote.items import QuoteItem

class QuoteSpider(scrapy.Spider):
    name = "quote"
    allowed_domains = ["quotes.toscrape.com"]
    start_urls = ["https://quotes.toscrape.com/page/1/"]

    def parse(self, response):
        quotes = Selector(response).xpath('//div[@class="quote"]')

        for quote in quotes:
            item = QuoteItem()
            item['text'] = quote.xpath('.//span[@class="text"]/text()').get()
            item['author'] = quote.xpath('.//small[@class="author"]/text()').get()
            item['author_url'] = quote.xpath('.//a[contains(@href, "author")]/@href').get()
            yield item


 # yield {
            #     'text': quote.xpath('//span[@class="text"]/text()').get(),
            #     'author': quote.xpath('//small[@class="author"]/text()').get(),
            #     'author_url': quote.xpath('//a[contains(@href, "author")]/@href').get(),
            #     'tags': quote.xpath('//div[@class="tags"]/a/text()').getall(),
            # }

# quotes = response.css('div.quote')
#         for quote in quotes:
#             yield {
#                 'text': quote.css('span.text::text').get(),
#                 'author': quote.css('small.author::text').get(),
#                 'tags': quote.css('div.tags a.tag::text').getall(),
#             }
