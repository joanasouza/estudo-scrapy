from scrapy.item import Item, Field


class QuoteItem(Item):
    text = Field()
    author = Field()
    author_url = Field(serializer=str)
    # tags = Field()


